<?php namespace Teamgrid\Project;

use Backend\Facades\Backend;
use Illuminate\Support\Facades\Event;
use System\Classes\PluginBase;
use Teamgrid\Project\Classes\Extend\ProjectBoot;
use Teamgrid\Project\Classes\Extend\UserExtend;

/**
 * Project Plugin Information File
 */
class Plugin extends PluginBase
{
    public function pluginDetails()
    {
        return [
            'name'        => 'Project',
            'description' => 'No description provided yet...',
            'author'      => 'Teamgrid',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {
    }

    public function boot()
    {
        Event::listen('backend.menu.extendItems', function ($manager) {
            $manager->removeMainMenuItem('October.Backend', 'media');
            $manager->removeMainMenuItem('October.Backend', 'dashboard');
        });

        ProjectBoot::generateProjectKey();

        UserExtend::userExtend();
    }


    public function registerNavigation()
    {
        return [
            'project' => [
                'label'       => 'Project',
                'url'         => Backend::url('teamgrid/project/projects'),
                'icon'        => 'icon-folder-open',
                'permissions' => ['teamgrid.project.*'],
                'order'       => 500,
            ],
        ];
    }
}
