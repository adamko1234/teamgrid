<?php

namespace Teamgrid\Project\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProjectResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'project_key'   => $this->project_key,
            'title'         => $this->title,

            'is_archived'  => $this->is_archived,
            'is_completed' => $this->is_completed,

            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,

            'customer_id' => $this->customer_id,
            'project_manager_id' => $this->project_manager_id
        ];
    }
}
