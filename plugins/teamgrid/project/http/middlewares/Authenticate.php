<?php

namespace Teamgrid\Project\Http\Middlewares;

use Closure;
use Teamgrid\Project\Models\Project;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;

/**
 * Handle an incoming request.
 *
 * @param \Illuminate\Http\Request $request
 * @param \Closure $next
 * @return mixed
 */

class Authenticate extends BaseMiddleware
{
    public function handle($request, Closure $next)
    {
        $id = $request->route('id');
        $project = Project::findOrFail($id);

        $userRole = auth()->user()->id->first()->pivot->role;

        if ($project->users()->where('user_id', $userRole) != ['project_manager', 'costumer']) {
            return response()->json(['error' => 'You do not have permission'], 401);
        }

        return $next($request);
    }
}
