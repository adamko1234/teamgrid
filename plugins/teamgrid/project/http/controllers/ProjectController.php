<?php

namespace Teamgrid\Project\Http\Controllers;

use Illuminate\Http\Request;
use Teamgrid\Project\Http\Resources\ProjectResource;
use Teamgrid\Project\Models\Project;

class ProjectController extends AbstractController
{
    public function createProject(): ProjectResource
    {
        $owner = auth()->user();

        $newProject = Project::create([
            'title' => post('title'),
            'is_completed' => post('is_completed'),
            'is_archived' => post('is_archived'),
        ]);

        $newProject->users()->attach($owner, ['role' => 'project_manager']);

        $newProject->save();

        return new ProjectResource($newProject);
    }

    public function deleteProject($project_key): ProjectResource
    {
        $this->isGranted($project_key, ['project_manager','maintainer']);

        $project = Project::findorFail($project_key);

        $project->delete();

        return new ProjectResource($project);
    }

    public function closeProject($project_key): ProjectResource
    {
        $this->isGranted($project_key, ['project_manager','maintainer']);

        $project = Project::findorFail($project_key);

        $project->is_completed = true;

        $project->save();

        return new ProjectResource($project);
    }

    public function getProject($project_key)
    {
        $this->isGranted($project_key, ['project_manager',  'customer', 'developer', 'maintainer']);

        return ProjectResource ::collection(Project::where('project_key', $project_key)->get());
    }

    public function getProjects($project_key)
    {
        $this->isGranted($project_key, ['project_manager', 'developer',  'maintainer']);

        return ProjectResource::collection(Project::all());
    }

    public function updateProject(Request $request, $project_key): ProjectResource
    {
        $this->isGranted($project_key, ['project_manager','maintainer','developer']);

        $project = Project::findorFail($project_key);

        $project->title = $request->input('title', $project->title);
        $project->is_archived = $request->input('is_archived', $project->is_archived);
        $project->is_completed = $request->input('is_completed', $project->is_completed);

        $project->save();

        return new ProjectResource($project);
    }
}
