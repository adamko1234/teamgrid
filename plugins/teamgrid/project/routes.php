<?php

use Teamgrid\Project\Http\Controllers\ProjectController;
use Teamgrid\Project\Http\Middlewares\Authenticate;

Route::group([
    'prefix' => 'api/v1/project',
    'middleware' => Authenticate::class,
], function () {
    Route::get('/', [ProjectController::class, 'getProjects']);
    Route::get('{projectKey}', [ProjectController::class, 'getProject']);
    Route::post('create', [ProjectController::class, 'createProject']);
    Route::patch('{projectKey}', [ProjectController::class, 'updateProject']);
    Route::delete('{projectKey}', [ProjectController::class, 'deleteProject']);
    Route::patch('close/{projectKey}', [ProjectController::class, 'closeProject']);
});
