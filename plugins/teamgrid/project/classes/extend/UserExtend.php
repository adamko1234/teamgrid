<?php

namespace Teamgrid\Project\Classes\Extend;

use Illuminate\Support\Facades\Event;
use RainLab\User\Models\User as UserModel;
use RainLab\User\Controllers\Users as UserController;
use Teamgrid\Project\Models\Project;
use Teamgrid\Task\Models\Task;
use Teamgrid\TimeEntry\Models\TimeEntry;

class UserExtend extends UserModel
{
    public static function userExtend()
    {
        UserModel::extend(function ($model) {
            $model->belongsToMany['projects'] = [
                Project::class,
                'pivot' => ['role'],
                'table' => 'project_user'
            ];
            $model->hasMany['tasks'] = [
                Task::class,
                'key' => 'user_id',
            ];
            $model->hasMany['timeentries'] = [
                TimeEntry::class,
                'key' => 'user_id',
            ];

            $model->addDynamicMethod('getRoleOptions', function () {
                return config('teamgrid.project::roles', []);
            });
        });

        UserController::extend(function ($controller) {
            if (!isset($controller->relationConfig)) {
                $controller->addDynamicProperty('relationConfig');
            }

            $configPaths = [
                '$/teamgrid/project/controllers/projects/config_userExtend.yaml',
                '$/teamgrid/task/controllers/tasks/config_userExtend.yaml',
                '$/teamgrid/timeentry/controllers/timeentries/config_userExtend.yaml',
            ];

            foreach ($configPaths as $configPath) {
                $controller->relationConfig = $controller
                    ->mergeConfig($controller->relationConfig ?? [], $configPath);
            }

            if (!$controller->isClassExtendedWith('Backend.Behaviors.RelationController')) {
                $controller->extendClassWith('Backend.Behaviors.RelationController');
            }
        });


        Event::listen('backend.form.extendFields', function ($widget) {

            if (!$widget->getController() instanceof UserController) {
                return;
            }

            if (!$widget->model instanceof UserModel) {
                return;
            }

            if ($widget->isNested) {
                return;
            }

            $widget->addTabFields([
                'projects' => [
                    'label' => 'Projects',
                    'tab'   => 'Projects',
                    'type'  => 'partial',
                    'path'  => '$/teamgrid/project/controllers/projects/_userExtendRelation.htm',
                ],
                 'tasks' => [
                    'label' => 'Tasks',
                    'tab'   => 'Tasks',
                    'type'  => 'partial',
                    'path'  => '$/teamgrid/task/controllers/tasks/_userExtendRelation.htm',
                ],
                'timeentries' => [
                    'label' => 'Time_entries',
                    'tab'   => 'Time_entries',
                    'type'  => 'partial',
                    'path'  => '$/teamgrid/timeentry/controllers/timeentries/_userExtendRelation.htm',
                ],
            ]);
        });
    }
}
