<?php namespace Teamgrid\Project\Classes\Extend;

use DateTime;
use Teamgrid\Project\Models\Project;

class ProjectBoot
{
    public static function generateProjectKey()
    {
        Project::Extend(function ($model) {
            $model->bindEvent('model.beforeCreate', function () use ($model) {
                if (!$model->exists && empty($model->project_key)) {
                    $projectId = $model->id;
                    $uniqueKey = new DateTime();
                    $uniqueKey = $uniqueKey->getTimestamp();
                    $uniqueKey .= $projectId;
                    $uniqueKey = hash('sha256', $uniqueKey);
                    $model->project_key = $uniqueKey;
                }
            });
        });
    }
}
