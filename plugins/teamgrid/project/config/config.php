<?php

return [
    'roles' => [
        'customer'        => 'Customer',
        'project_manager' => 'Project Manager',
        'developer'       => 'Developer',
        'maintainer'      => 'Maintainer',
    ]
];
