<?php

namespace Teamgrid\Project\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateProjectsUsersTable extends Migration
{
    /**
     * up builds the migration
     */
    public function up()
    {
        Schema::create('project_user', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->primary(['user_id', 'project_id']);
            $table->integer('user_id')->unsigned();
            $table->integer('project_id')->unsigned();
            $table->string('role')->nullable();
            $table->timestamps();
        });
    }

    /**
     * down reverses the migration
     */
    public function down()
    {
        Schema::dropIfExists('project_user');
    }
};
