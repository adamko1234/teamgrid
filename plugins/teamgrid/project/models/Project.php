<?php

namespace Teamgrid\Project\Models;

use Model;
use RainLab\User\Models\User as User;
use Teamgrid\Task\Models\Task;

/**
 * Project Model
 */
class Project extends Model
{
    use \October\Rain\Database\Traits\Validation;

    public $table = 'teamgrid_project_projects';

    protected $guarded = ['*'];

    protected $fillable = [
        'project_key',
        'title',
        'is_archived',
        'is_completed',
    ];

    public $rules = [
        'title' => 'required'
    ];

    protected $casts = [
        'is_archived' => 'boolean',
        'is_completed' => 'boolean',
    ];

    protected $jsonable = [];

    protected $appends = [];

    protected $hidden = [];

    protected $dates =[
        'created_at',
        'updated_at',
    ];


    public $hasMany = [
        'tasks' => [
            Task::class,
        ],
    ];

    public $belongsTo = [
        'project_manager' => ['User'],
        'customer'        => ['User'],
        'maintainer'      => ['User'],
        'developer'       => ['User'],
    ];
    public $belongsToMany = [
        'users' => [
            User::class,
            'project_user',
            'pivot' => ['role'],
            'table' => 'project_user'
        ],
    ];
}
