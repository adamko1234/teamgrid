<?php

namespace Teamgrid\Timeentry\Http\resources;

use Illuminate\Http\Resources\Json\Resource;

class TimeEntryResource extends Resource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,

            'project_id' => $this->project_id,
            'task_id' => $this->task_id,

            'start_time' => $this->start_time,
            'end_time' => $this->end_time,
            'total_time' => $this->total_time,
        ];
    }
}
