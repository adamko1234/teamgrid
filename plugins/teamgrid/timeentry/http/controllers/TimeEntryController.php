<?php

namespace Teamgrid\Timeentry\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Request;
use Teamgrid\Timeentry\Http\Resources\TimeEntryResource;
use Teamgrid\TimeEntry\Models\TimeEntry;

class TimeEntryController extends AbstractController
{
    public function startTracking($id): TimeEntryResource
    {
        $this->isGranted($id, ['project_manager', 'developer', 'maintainer']);
        $timeEntry = TimeEntry::findorFail($id);

        $timeEntry->start_time = now('Europe/Bratislava');

        $timeEntry->save();

        return new TimeEntryResource($timeEntry);
    }

    public function stopTracking($id): TimeEntryResource
    {
        $this->isGranted($id, ['project_manager', 'developer', 'maintainer']);

        $timeEntry = TimeEntry::findorFail($id);

        $timeEntry->end_time = now('Europe/Bratislava');

        $timeEntry->save();

        return new TimeEntryResource($timeEntry);
    }

    public function createTimeEntry($id): TimeEntryResource
    {
        $this->isGranted($id, ['project_manager', 'developer', 'maintainer']);

        $newTimeEntry = TimeEntry::create([
            'user_id' => post('user_id'),
            'task_id' => post('task_id'),
            'start_time' => post('start_time', now('Europe/Bratislava')),
            'end_time' => post('end_time'),
        ]);

        $newTimeEntry->save();

        return new TimeEntryResource($newTimeEntry);
    }
    public function updateTimeEntry(Request $request, $id): TimeEntryResource
    {
        $this->isGranted($id, ['project_manager', 'developer', 'maintainer']);

        $timeEntry = TimeEntry::findorFail($id);

        $timeEntry->start_time = $request->input('start_time', $timeEntry->start_time);
        $timeEntry->end_time = $request->input('end_time', $timeEntry->end_time);

        $timeEntry->save();

        return new TimeEntryResource($timeEntry);
    }

}
