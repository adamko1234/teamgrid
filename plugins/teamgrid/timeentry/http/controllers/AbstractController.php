<?php

namespace Teamgrid\Timeentry\Http\Controllers;

use Illuminate\Routing\Controller;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Teamgrid\Project\Models\Project;

class AbstractController extends Controller
{
    /**
     * @param Project|null $project_key
     * @param array $roles
     * @param bool $return
     * @return bool|void
     */
    protected function isGranted(?Project $project_key, array $roles, bool $return = false)
    {
        $projectUser = auth()->user() ? $project_key->users->where('id', auth()->user()->id)->first() : null;
        $granted = $projectUser
            && in_array($projectUser->pivot->role, $roles);
        if ($return) {
            return $granted;
        } elseif (!$granted) {
            throw new AccessDeniedHttpException('Access denied!');
        }
    }
}
