<?php

use Teamgrid\Timeentry\Http\Controllers\TimeEntryController;
use Teamgrid\Timeentry\Http\middlewares\Authenticate;

Route::group([
    'prefix' => 'api/v1/timeentry',
    'middleware' => Authenticate::class,
], function () {
    Route::post('{id}/start', [TimeEntryController::class], 'startTracking');
    Route::post('{id}/stop', [TimeEntryController::class], 'stopTracking');
    Route::post('create', [TimeEntryController::class], 'createTimeEntry');
    Route::patch('{id}/update', [TimeEntryController::class], 'updateTimeEntry');
});
