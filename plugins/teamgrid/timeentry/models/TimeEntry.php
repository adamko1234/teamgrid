<?php namespace Teamgrid\TimeEntry\Models;

use Model;
use RainLab\User\Models\User;

/**
 * TimeEntry Model
 */
class TimeEntry extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'teamgrid_timeentry_time_entries';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Validation rules for attributes
     */
    public $rules = [];

    /**
     * @var array Attributes to be cast to native types
     */
    protected $casts = [];

    /**
     * @var array Attributes to be cast to JSON
     */
    protected $jsonable = [];

    /**
     * @var array Attributes to be appended to the API representation of the model (ex. toArray())
     */
    protected $appends = [];

    /**
     * @var array Attributes to be removed from the API representation of the model (ex. toArray())
     */
    protected $hidden = [];

    /**
     * @var array Attributes to be cast to Argon (Carbon) instances
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'start_time',
        'end_time',
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $hasOneThrough = [];
    public $hasManyThrough = [];
    public $belongsTo = [
        'task' => 'Teamgrid\Task\Models\Task',
        'user' => 'RainLab\User\Models\User',
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function getTotalTimeAttribute()
    {
        $options = [];

        $user = User::find($this->user_id);
        if (!$user) {
            return $options;
        }

        $timeEntries = $user->timeEntries;
        if (is_iterable($timeEntries)) {
            foreach ($timeEntries as $timeEntry) {
                if ($timeEntry->start_time && $timeEntry->end_time) {
                    $start_time = strtotime($timeEntry->start_time);
                    $end_time = strtotime($timeEntry->end_time);

                    if ($start_time !== false && $end_time !== false) {
                        $total_time = $end_time - $start_time;
                        $options[$timeEntry->id] = $total_time;
                    }
                }
            }
        }
        return $options;
    }
}
