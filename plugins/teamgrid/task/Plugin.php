<?php namespace Teamgrid\Task;

use Backend;
use RainLab\User\Models\User;
use System\Classes\PluginBase;
use Teamgrid\Task\Models\Task;

/**
 * Task Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Task',
            'description' => 'No description provided yet...',
            'author'      => 'Teamgrid',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
//        User::extend(function ($model) {
//            $model->hasMany['tasks' => Task::class,
//            ];
//        });
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Teamgrid\Task\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'teamgrid.task.some_permission' => [
                'tab' => 'Task',
                'label' => 'Some permission'
            ],
        ];
    }

    public function registerNavigation()
    {

        return [
            'task' => [
                'label'       => 'Task',
                'url'         => Backend::url('teamgrid/task/tasks'),
                'icon'        => 'icon-plus',
                'permissions' => ['teamgrid.task.*'],
                'order'       => 500,
            ],
        ];
    }
}
