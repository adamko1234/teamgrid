<?php namespace Teamgrid\Task\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateTasksTable extends Migration
{
    public function up()
    {
        Schema::create('teamgrid_task_tasks', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->string('name');
            $table->string('description')->nullable()->index()->default(input('description'));

            $table->integer('project_id')->nullable();
            $table->string('user_id')->nullable();

            $table->boolean('is_completed')->default(false)->index();

            $table->time('tracked_time')->nullable();
            $table->dateTimeTz('planned_start')->nullable();
            $table->dateTimeTz('planned_end')->nullable();

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('teamgrid_task_tasks');
    }
}
