<?php namespace Teamgrid\Task\Models;

use Model;
use RainLab\User\Models\User;
use Teamgrid\Project\Models\Project;
use Teamgrid\Timeentry\Models\Timeentry;

/**
 * Task Model
 */
class Task extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'teamgrid_task_tasks';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'name'
    ];

    /**
     * @var array Validation rules for attributes
     */
    public $rules = [];

    /**
     * @var array Attributes to be cast to native types
     */
    protected $casts = [
        'is_completed' => 'boolean',
        'tracked_time' => 'time',
        'planned_start' => 'datetime',
        'planned_end' => 'datetime',
    ];

    /**
     * @var array Attributes to be cast to JSON
     */
    protected $jsonable = [];

    /**
     * @var array Attributes to be appended to the API representation of the model (ex. toArray())
     */
    protected $appends = [];

    /**
     * @var array Attributes to be removed from the API representation of the model (ex. toArray())
     */
    protected $hidden = [];

    /**
     * @var array Attributes to be cast to Argon (Carbon) instances
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'timeentries' => [
            Timeentry::class,
            'key' => 'id',
        ],
    ];
    public $hasOneThrough = [];
    public $hasManyThrough = [];
    public $belongsTo = [
        'project' => Project::class,
        'user'    => User::class,
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function getProjectOptions(): array
    {
        $options = [];
        $projects = Project::all();

        foreach ($projects as $project) {
            $options[$project->id] = $project->title;
        }

        return $options;
    }
    public function getAssigneeOptions(): array
    {
        $options = [];
        $project = $this->project;
        if (!$project) {
            return $options;
        }
        $users = $this->project->users;

        foreach ($users as $user) {
            $options[$user->id] = $user->name;
        }

        return $options;
    }
}
