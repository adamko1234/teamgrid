<?php

use Teamgrid\Task\Http\Controllers\TaskController;
use Teamgrid\Task\Http\middlewares\Authenticate;

Route::group([
    'prefix' => 'api/v1/task',
    'middleware' => Authenticate::class,
], function () {
    Route::get('{id}', [TaskController::class, 'getTask']);
    Route::post('create', [TaskController::class, 'createTask']);
    Route::patch('{id}', [TaskController::class, 'updateTask']);
    Route::patch('{id}/complete', [TaskController::class, 'completeTask']);
});
