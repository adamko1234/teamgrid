<?php

namespace Teamgrid\Task\Http\Middlewares;

use Closure;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Teamgrid\Project\Models\Project;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;

/**
 * Handle an incoming request.
 *
 * @param \Illuminate\Http\Request $request
 * @param \Closure $next
 * @return mixed
 */

class Authenticate extends BaseMiddleware
{
    public function handle($request, Closure $next, ?Project $project_key, array $roles, bool $return = false)
    {
        $projectUser = auth()->user() ? $project_key->users->where('id', auth()->user()->id)->first() : null;
        $granted = $projectUser
            && in_array($projectUser->pivot->role, $roles);
        if ($return) {
            return $granted;
        } elseif (!$granted) {
            throw new AccessDeniedHttpException('Access denied!');
        }
    }
}
