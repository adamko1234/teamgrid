<?php

namespace Teamgrid\Task\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TaskResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id'      => $this->id,
            'project' => $this->project_id,

            'name'       => $this->name,
            'description' => $this->description,

            'is_completed' => $this->is_completed,

            'tracked_time' => $this->tracked_time,
            'planned_start' => $this->planned_start,
            'planned_end' => $this->planned_end,

        ];
    }
}
