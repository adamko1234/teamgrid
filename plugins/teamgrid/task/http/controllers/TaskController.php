<?php

namespace Teamgrid\Task\Http\Controllers;

use Illuminate\Http\Request;
use Teamgrid\Task\Http\Resources\TaskResource;
use Teamgrid\Task\Models\Task;

class TaskController extends AbstractController
{
    public function completeTask($task): TaskResource
    {
        $this->isGranted($task, ['project_manager', 'developer', 'maintainer']);

        $task = Task::findorFail($task);

        $task->is_completed = true;

        $task->save();

        return new TaskResource($task);
    }

    public function createTask($task): TaskResource
    {
        $this->isGranted($task, ['project_manager', 'developer', 'maintainer']);

        $newTask = Task::create([
            'name' => post('name'),
            'is_completed' => post('is_completed', 0),
            'project_id' => post('project_id'),
            'description' => post('description'),
            'created_at' => post('created_at', now('Europe/Bratislava')),
            'updated_at' => post('updated_at'),
        ]);

        $newTask->save();

        return new TaskResource($newTask);
    }

    public function updateTask(Request $request, $task): TaskResource
    {
        $this->isGranted($task, ['project_manager', 'developer', 'maintainer']);

        $task = Task::findorFail($task);

        $task->name = $request->input('name', $task->name);
        $task->is_completed = $request->input('is_completed', $task->is_completed);
        $task->description = $request->input('description', $task->description);
        $task->updated_at = $request->input('updated_at', now('Europe/Bratislava'));

        $task->save();

        return new TaskResource($task);
    }

    public function getTask($task): TaskResource
    {
        $this->isGranted($task, ['project_manager', 'customer', 'developer', 'maintainer']);

        $task = Task::findorFail($task);

        return new TaskResource($task);
    }
}
